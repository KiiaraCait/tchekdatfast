﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateObjectUsingSprite : StateMachine
{
    private bool isLoop = false;
    private Sprite[] listOfSprites;
    [SerializeField] private float ratioTime = 1 / 60;
    private float timeCounter;
    private int currentIndex = 0;
    [SerializeField]  private SpriteRenderer spriteRenderer;

    public void AnimationMode(List<Sprite> pListOfSprites, bool pIsBool)
    {
        listOfSprites = pListOfSprites.ToArray();
        isLoop = pIsBool;
        currentIndex = 0;
        spriteRenderer.sprite = listOfSprites[currentIndex];

        SetModeAnimation();
    }

    private void SetModeAnimation()
    {
        doAction = DoActionAnimation;
    }

    private void DoActionAnimation()
    {
        timeCounter += Time.deltaTime;

        if(timeCounter >= ratioTime)
        {
            timeCounter = 0;

            currentIndex++;

            if(currentIndex == listOfSprites.Length)
            {
                if (isLoop) currentIndex = 0;

                else
                {
                    VoidMode();
                    return;
                }
            }

            spriteRenderer.sprite = listOfSprites[currentIndex];
        }
    }

    private void Update()
    {
        doAction();
    }
}
