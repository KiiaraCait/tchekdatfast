﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class StateMachine : MonoBehaviour
{
    public Action doAction{ get; protected set; }

   virtual protected void Awake()
   {
        SetModeVoid(); 
   }

    virtual public void NormalMode()
    {
        SetModeNormal(); 
    }

    virtual public void VoidMode()
    {
        SetModeVoid();
    }

    private void SetModeVoid()
    {
        doAction = DoActionVoid; 
    } 
    private void SetModeNormal()
    {
        doAction = DoActionNormal; 
    }

    virtual protected void DoActionVoid()
    {
    }
    
    virtual protected void DoActionNormal()
    {
    }
}
