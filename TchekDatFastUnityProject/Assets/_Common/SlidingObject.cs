﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingObject : StateMachine
{
    private Transform target;
    private Action onComplete;
    private float speed;
    private bool localPos;

    private float speedCoef = 1f;

    private Vector3 slidingFinalPos;
    private List<Vector3> waypoints;

    /// <summary>
    /// Permet à l'objet d'effectuer un slide de sa position actuelle jusqu'à un point.
    /// </summary>
    /// <param name="pTarget">L'objet en question.</param>
    /// <param name="pSlidingFinalPos">La position visée.</param>
    /// <param name="pOnComplete">La fonction à appeler une fois le slide fini, par exemple le mode normal ou void.</param>
    /// <param name="pSpeed">La vitesse de déplacement (300 par défaut).</param>
    /// <param name="pLocalPos">Indique si le déplacement doit se faire dans le repère local ou global.</param>
    virtual public void SlidePointMode(Transform pTarget, Vector3 pSlidingFinalPos, Action pOnComplete, float pSpeed = 300f, bool pLocalPos = false)
    {
        target = pTarget;
        slidingFinalPos = pSlidingFinalPos;
        onComplete = pOnComplete;
        speed = pSpeed;
        localPos = pLocalPos;

        setModeSlidePoint();
    }

    /// <summary>
    /// Permet à l'objet d'effectuer une suite de slides à partir de sa position actuelle, en allant à des points indiqués.
    /// </summary>
    /// <param name="pTarget">L'objet en question.</param>
    /// <param name="pWaypoints">La liste de positions souhaitées.</param>
    /// <param name="pOnComplete">La fonction à appeler une fois le slide fini, par exemple le mode normal ou void.</param>
    /// <param name="pSpeed">La vitesse de déplacement (300 par défaut).</param>
    /// <param name="pLocalPos">Indique si le déplacement doit se faire dans le repère local ou global.</param>
    virtual public void SlidePointsMode(Transform pTarget, List<Vector3> pWaypoints, Action pOnComplete, float pSpeed = 300f, bool pLocalPos = false)
    {
        target = pTarget;
        waypoints = pWaypoints;
        onComplete = pOnComplete;
        speed = pSpeed;
        localPos = pLocalPos;

        setModeSlidePoints();
    }

    private void setModeSlidePoint()
    {
        doAction = DoActionSlidePoint;
    }

    private void setModeSlidePoints()
    {
        doAction = DoActionSlidePoints;
    }

    virtual protected void DoActionSlidePoint()
    {
        speedCoef += 0.2f;

        if (localPos)
        {
            target.localPosition = Vector3.MoveTowards(target.localPosition, slidingFinalPos, speed * speedCoef *  Time.deltaTime);

            if (target.localPosition == slidingFinalPos) onComplete();
        }
        else
        {
            target.position = Vector3.MoveTowards(target.position, slidingFinalPos, speed * speedCoef * Time.deltaTime);

            if (target.position == slidingFinalPos) onComplete();
        }
    }

    virtual protected void DoActionSlidePoints()
    {
        Vector3 nextPosition = waypoints[0];

        if (localPos)
        {
            target.localPosition = Vector3.MoveTowards(target.localPosition, nextPosition, speed * speedCoef * Time.deltaTime);

            if (target.localPosition == nextPosition)
            {
                waypoints.RemoveAt(0);

                if (waypoints.Count == 0) onComplete();
            }
        }
        else
        {
            target.position = Vector3.MoveTowards(target.position, nextPosition, speed * speedCoef * Time.deltaTime);

            if (target.position == nextPosition)
            {
                waypoints.RemoveAt(0);

                if (waypoints.Count == 0) onComplete();
            }
        }
    }
}
