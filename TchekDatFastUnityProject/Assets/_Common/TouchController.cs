﻿using UnityEngine;
using UnityEngine.Events;

public class TouchController : MonoBehaviour
{
    public static UnityEvent onTouchBegin = new UnityEvent();
    public static UnityEvent onTouchEnd = new UnityEvent();

    private static TouchController instance;

    private Touch mainTouch;
    private bool hasTouched = false;
    private int fingersWhenReleased = 0;

    public static TouchController GetInstance()
    {
        return instance;
    }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
    }

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR

    void Update()
    {
        if (Input.touchCount > 0)
        {
            if (!hasTouched)
            {
                mainTouch = Input.GetTouch(0);
                onTouchBegin.Invoke();
                hasTouched = true;
            }
            else if(mainTouch.fingerId == Input.GetTouch(0).fingerId)
            {
                mainTouch = Input.GetTouch(0);
            }
            else if(fingersWhenReleased == 0)
            {
                fingersWhenReleased = Input.touchCount;
                onTouchEnd.Invoke();
            }

            if (fingersWhenReleased != 0 && fingersWhenReleased < Input.touchCount)
            {
                hasTouched = false;
                fingersWhenReleased = 0;
            }

        }else if (fingersWhenReleased == 0 && hasTouched && mainTouch.phase == TouchPhase.Ended){
            onTouchEnd.Invoke();
            hasTouched = false;
        }
        else
        {
            fingersWhenReleased = 0;
            hasTouched = false;
        }
    }

    public Vector3 GetTouchPosition()
    {
        if (mainTouch.phase == TouchPhase.Ended)
            return Vector3.zero;

        return mainTouch.position;
    }

#elif UNITY_EDITOR

    void Update()
    {
         if (Input.GetMouseButtonDown(0))
         {
            onTouchBegin.Invoke();
         }

        else if (Input.GetMouseButtonUp(0))
            onTouchEnd.Invoke();
    }

     public Vector3 GetTouchPosition()
     {
        return Input.mousePosition;
     }

#endif


    private void OnDestroy()
    {
        if (instance == this)
            instance = null;
    }
}
