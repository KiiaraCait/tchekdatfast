﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Managers
{
    uiManager,
    levelManager,
    soundManager,
    endLevelManager,
    dataManager,
    timelineManager,

}
