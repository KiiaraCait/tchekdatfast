﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "Level", menuName = "ScriptableObject/Levels/Level", order = 1)]

public class Level : ScriptableObject
{
    //List
    [SerializeField] private List<ActionToDo> list = new List<ActionToDo>();

}
