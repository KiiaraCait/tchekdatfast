﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "Level", menuName = "ScriptableObject/Levels/ActionToDo", order = 2)]
public class ActionToDo : ScriptableObject
{

    [SerializeField] private GameObject aspect;
    [SerializeField] private HandFamilies type;
    [SerializeField] private float maxDuration = 3;

}
