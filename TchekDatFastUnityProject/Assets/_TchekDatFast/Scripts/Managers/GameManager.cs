﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private List<Manager> managers = new List<Manager>();


    public Manager GetManager(Managers managerName)
    {
        int length = managers.Count;
        Manager manager;

        for (int i = 0; i < length; i++)
        {
            manager = managers[i];

            if(manager.GetName == managerName)
            {
                return manager;
            }
        }

        return managers[0];
    }
}
